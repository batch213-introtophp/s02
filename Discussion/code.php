<?php

// Repetition Control Structures
// While loop:

function whileLoop(){
	$count = 5;

	while ($count !== 0){
		echo $count . '<br/>';
		$count--;
	}
}

// Do while loop:

function doWhileLoop(){
	$count = 20;

	do{
		echo $count . '<br/>';
		$count--;
	}while ($count > 0);
}

// For Loop:

function forLoop(){
	for($count = 0; $count <= 20; $count++){
		echo $count . '<br/>';
	}
};

// Array Manipulation
// PHP Arrays are declared using square brackets '[]' or array() function.

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //introduce before PHP 5.4

$newStudentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; // introduce on PHP 5.4

// Simple Arrays

$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo','Neo', 'Redfox', 'Gateway', 'Toshiba'];


// Associative array

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 90.1, 'thirdGrading' => 94.3, 'fourthGrading' => 89.2];